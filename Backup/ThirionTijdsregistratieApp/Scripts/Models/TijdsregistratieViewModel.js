﻿function GetTijdsregistraties() {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistraties');
        var currentdate = new Date();
        var datum = "";
        if ($("#datumTextBox").val() !== "")
            datum = ToSharePointLookupDate($("#datumTextBox").val());
        else
            datum = currentdate.getFullYear() + "-" + ((currentdate.getMonth() + 1) < 10 ? "0" + (currentdate.getMonth() + 1) : (currentdate.getMonth() + 1)) + "-" + (currentdate.getDate() < 10 ? "0" + currentdate.getDate() : currentdate.getDate()) + "T00:00:00";

        var caml = "<View>";
        caml += "<Query>";
        caml += "<Where>";
        //caml += "<And>";
        caml += "<Eq><FieldRef Name='Startuur' /><Value Type='DateTime'>" + datum + "</Value></Eq>";
        //caml += "<Leq><FieldRef Name='Einduur' /><Value Type='DateTime'>" + tot + "</Value></Leq>";
        //caml += "</And>";
        caml += "</Where>";
        caml += "<OrderBy><FieldRef Name='Startuur' /></OrderBy>";
        caml += "</Query>";
        caml += "<ViewFields>";
        caml += "<FieldRef Name='Klant' />";
        caml += "<FieldRef Name='Startuur' />";
        caml += "<FieldRef Name='Einduur' />";
        caml += "<FieldRef Name='Author' />";
        caml += "<FieldRef Name='Activiteit' />";
        caml += "<FieldRef Name='Opdracht'/>";
        caml += "<FieldRef Name='Title' />";
        caml += "<FieldRef Name='Omschrijving' />";
        caml += "<FieldRef Name='Gefactureerd' />";
        caml += "<FieldRef Name='Duur' />";
        caml += "<FieldRef Name='Activiteit_x003a_Tarief' />";
        caml += "</ViewFields>";
        caml += "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var registraties = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var mymedewerker = item.get_item('Author').get_lookupValue();
            var myactiviteit = "";
            if (item.get_item('Activiteit') !== null)
                myactiviteit = item.get_item('Activiteit').get_lookupValue();
            var mytarief = "";
            if (item.get_item('Activiteit_x003a_Tarief') !== null)
                mytarief = item.get_item('Activiteit_x003a_Tarief').get_lookupValue();
            registraties.push({
                Klant: item.get_item('Klant').get_lookupValue(),
                Startuur: ToDateString(item.get_item('Startuur'), false),
                Einduur: ToDateString(item.get_item('Einduur'), false),
                Medewerker: mymedewerker,
                Activiteit: myactiviteit,
                Opdracht: item.get_item('Opdracht') == null ? "" : item.get_item('Opdracht'),
                Omschrijving: item.get_item('Omschrijving'),
                Duur: item.get_item('Duur'),
                Tarief: mytarief
            })
        }
        BindList(registraties);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de registraties niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindList(registraties) {
    var myBody = $("#registratieTbody");
    myBody.empty();
    var totaal = 0;
    if (registraties.length > 0) {
        for (var i = 0; i < registraties.length; i++) {
            var row = "<tr><td style='padding:5px 10px;'>" + registraties[i].Activiteit + "</td>";
            row += "<td style='padding:5px 10px;'>" + registraties[i].Opdracht + "</td>";
            row += "<td style='padding:5px 10px;'>" + registraties[i].Klant + "</td>";
            row += "<td style='padding:5px 10px;'>" + registraties[i].Startuur + "</td>";
            row += "<td style='padding:5px 10px;'>" + registraties[i].Einduur + "</td>";
            row += "<td style='padding:5px 10px;'>" + registraties[i].Duur.toFixed(2) + "</td>";
            row += "<td style='padding:5px 10px;'>" + registraties[i].Omschrijving + "</td>";
            //row += "<td>" + registraties[i].Gefactureerd + "</td>";
            row += "</tr>";
            myBody.append(row);
            totaal += parseFloat(registraties[i].Duur);
        }
        myBody.append("<tr><td colspan='7' style='border-bottom:2px solid black;'></td></tr>")
        myBody.append("<tr><td style='padding:5px 10px;'></td><td style='padding:5px 10px;'></td><td style='padding:5px 10px;'></td><td style='padding:5px 10px;'></td><td style='padding:5px 10px;'><b>Totaal duur</b></td><td style='padding:5px 10px;'><b>" + totaal.toFixed(2) + "</b></td><td style='padding:5px 10px;'></td></tr>")
    }
}

function updateButton_Click() {
    GetTijdsregistraties();
}

//***************** Registraties opslaan *************************/

function OpslaanButton_Click() {
    if (ValidateRegistratie()) {
        var registratie = GetRegistratieFromForm();
        SaveRegistratie(registratie);
    }
}

function ValidateRegistratie() {
    var valid = true;
    var errortext = "";
    if ($("#startuurTextBox").val() === "") {
        valid = false;
        errortext += "- Startdatum is niet ingevuld!\n";
    }
    if ($("#einduurTextBox").val() === "") {
        valid = false;
        errortext += "- Einddatum is niet ingevuld!\n";
    }
    if ($("#omschrijvingTextarea").val() === "") {
        valid = false;
        errortext += "- Omschrijving is niet ingevuld!\n";
    }
    if ($("#klantSelect option:selected").text() === "") {
        valid = false;
        errortext += "- Er is geen klant gekozen!\n";
    }
    if (!valid) {
        alert(errortext);
    }
    return valid;
}

function GetRegistratieFromForm() {
    var startuur = ToSharePointDate($("#startuurTextBox").val() + " " + $("#startuurSelect option:selected").text() + $("#startminuutSelect option:selected").text() + ":00");
    var einduur = ToSharePointDate($("#einduurTextBox").val() + " " + $("#einduurSelect option:selected").text() + $("#eindminuutSelect option:selected").text() + ":00");
    var registratie = {};
    if ($("#activiteitSelect option:selected").text() !== "(Geen)")
        registratie.Activiteit = $("#activiteitSelect option:selected").val();
    else
        registratie.Activiteit = "";
    if ($("#opdrachtSelect option:selected").text() !== "(Geen)")
        registratie.Opdracht = $("#opdrachtSelect option:selected").text();
    else
        registratie.Opdracht = "";
    registratie.Klant = $("#klantSelect option:selected").val();
    registratie.Startuur = startuur;
    registratie.Einduur = einduur;
    registratie.Omschrijving = $("#omschrijvingTextarea").val();
    //registratie.Gefactureerd = $("#gefactureerdCheckBox").is(":checked") == true ? "1" : "0";
    return registratie;
}

function SaveRegistratie(registratie) {
    var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
    var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistraties');

    var itemCreateInfo = new SP.ListItemCreationInformation();
    var item = list.addItem(itemCreateInfo);

    item.set_item("Startuur", registratie.Startuur);
    item.set_item("Einduur", registratie.Einduur);
    item.set_item("Klant", registratie.Klant);
    item.set_item("Omschrijving", registratie.Omschrijving);
    if (registratie.Opdracht !== "")
        item.set_item("Opdracht", registratie.Opdracht);
    if (registratie.Activiteit !== "")
        item.set_item("Activiteit", registratie.Activiteit);
    //item.set_item("Gefactureerd", registratie.Gefactureerd);

    item.update();
    context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
}

function onQuerySucceeded() {
    ClearRegistratie();
    GetTijdsregistraties();
}

function onQueryFailed(sender, args) {
    alert("Fout bij het opslaan." + args.get_message() + "\n" + args.get_stackTrace());
}

function ClearRegistratie() {
    $("#omschrijvingTextarea").val("");
    $("#activiteitSelect").val('0');
    $("#opdrachtSelect").val('0');
    $("#klantSelect").val('0');
}

function GetOpdrachten() {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        this.list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistraties');
        this.fields = list.get_fields();

        this.opdrachten = clientContext.castTo(list.get_fields().getByInternalNameOrTitle("Opdracht"), SP.FieldChoice);

        clientContext.load(list);
        clientContext.load(fields);
        clientContext.load(opdrachten);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var context = new SP.ClientContext.get_current();
        // Converting the Field to SPFieldChoice from the execution results
        var myChoicesfield = context.castTo(this.fields.getByInternalNameOrTitle("Opdracht"), SP.FieldChoice);
        //get_choices() method will return the array of choices provided in the field

        var choices = myChoicesfield.get_choices();

        var opdrachtselect = $("#opdrachtSelect");
        opdrachtselect.empty();
        opdrachtselect.append("<option value='0'>(Geen)</option>");
        if (choices.length > 0) {
            for (var i = 0; i < choices.length; i++) {
                opdrachtselect.append("<option value='" + (i+1) + "'>" + choices[i] + "</option>");
            }
        }
        GetTijdsregistraties();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de opdrachten niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}


