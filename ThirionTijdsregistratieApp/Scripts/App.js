﻿'use strict';

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var drempelwaarde;

// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    setDatePickers();
    var drempel = GetDrempelwaarde();
    drempel.done(function () {
        getUserName();
    })
});

// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {
    $('#message').text('Hello ' + user.get_title());
    GetActiviteiten();
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function setDatePickers() {
    $.datepicker.setDefaults($.datepicker.regional["nl"]);
    var date = new Date();

    var datep = $(".datepicker");
    datep.datepicker({
        'dateFormat': 'dd/mm/yy'//,
        //maxDate: new Date()
    });

    datep.each(function () {
        var $d = $(this);
        $d.val(ToDateString(date, true));
    });

    var hour = date.getHours();
    var later = date.getHours() + 1;

    $("#startuurSelect").val(hour < 10 ? "0" + hour + ":" : hour + ":");
    $("#einduurSelect").val(later < 10 ? "0" + later + ":" : later + ":");
}
