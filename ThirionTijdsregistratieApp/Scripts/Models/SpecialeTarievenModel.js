﻿function GetSpecialeTarieven() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Speciale tarieven');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><ViewFields><FieldRef Name='ID' /><FieldRef Name='Title' /><FieldRef Name='Tarief' /></ViewFields></View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Omschrijving: item.get_item('Title'),
                Tarief: item.get_item('Tarief')
            });
        }
        BindData(arraylist);
        GetActiviteiten();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de speciale tarieven niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindData(arraylist) {
    for (var i = 0; i < arraylist.length; i++) {
        if (arraylist[i].Omschrijving === "Default")
            DefaultTarief = arraylist[i].Tarief;
        else if (arraylist[i].Omschrijving === "isabelle Geysmans Fiscaliteit en Accountancy")
            IsabelleTarief = arraylist[i].Tarief;
        else
            JaakTarief = arraylist[i].Tarief;
    }
}
