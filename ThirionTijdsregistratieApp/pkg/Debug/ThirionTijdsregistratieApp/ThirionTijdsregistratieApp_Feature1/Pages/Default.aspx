﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <script src="../Scripts/Models/Drempelwaarde.js"></script>
    <script type="text/javascript" src="../Scripts/DatumFuncties.js"></script>
    <script type="text/javascript" src="../Scripts/datepicker-nl.js"></script>
    <script type="text/javascript" src="../Scripts/Models/ActiviteitenViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Models/KlantenViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Models/TijdsregistratieViewModel.js"></script>
    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />
    <link href="../Content/jquery-ui.min.css" rel="stylesheet" />
    <link href="../Content/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../Content/jquery-ui.theme.min.css" rel="stylesheet" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/App.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Tijdsregistratie App
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">

    <div>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
    </div>
    <h3>Invoer tijdsregistratie</h3>
    <div class="blok">
        <fieldset class="myfields">
            <div class="fieldDiv">
                <label class="labels" for="activiteitSelect">Activiteit</label>
                <select id="activiteitSelect" class="longselect"></select>
            </div>
            <div class="fieldDiv">
                <label class="labels" for="opdrachtSelect">Opdracht</label>
                <select id="opdrachtSelect" class="longselect">

                </select>
            </div>
            <div class="fieldDiv">
                <label class="labels" for="klantSelect">Klant</label>
                <select id="klantSelect" class="longselect"></select>
            </div>
            <div class="fieldDiv">
                <label class="labels" for="startuurTextBox">Startuur</label>
                <input type="text" id="startuurTextBox" class="datepicker" />
                <select id="startuurSelect">
                    <option>00:</option>
                    <option>01:</option>
                    <option>02:</option>
                    <option>03:</option>
                    <option>04:</option>
                    <option>05:</option>
                    <option>06:</option>
                    <option>07:</option>
                    <option>08:</option>
                    <option>09:</option>
                    <option>10:</option>
                    <option>11:</option>
                    <option>12:</option>
                    <option>13:</option>
                    <option>14:</option>
                    <option>15:</option>
                    <option>16:</option>
                    <option>17:</option>
                    <option>18:</option>
                    <option>19:</option>
                    <option>20:</option>
                    <option>21:</option>
                    <option>22:</option>
                    <option>23:</option>
                </select>
                <select id="startminuutSelect">
                    <option>00</option>
                    <option>05</option>
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                    <option>30</option>
                    <option>35</option>
                    <option>40</option>
                    <option>45</option>
                    <option>50</option>
                    <option>55</option>
                </select>
            </div>
            <div class="fieldDiv">
                <label class="labels" for="einduurTextBox">Einduur</label>
                <input type="text" id="einduurTextBox" class="datepicker" />
                <select id="einduurSelect">
                    <option>00:</option>
                    <option>01:</option>
                    <option>02:</option>
                    <option>03:</option>
                    <option>04:</option>
                    <option>05:</option>
                    <option>06:</option>
                    <option>07:</option>
                    <option>08:</option>
                    <option>09:</option>
                    <option>10:</option>
                    <option>11:</option>
                    <option>12:</option>
                    <option>13:</option>
                    <option>14:</option>
                    <option>15:</option>
                    <option>16:</option>
                    <option>17:</option>
                    <option>18:</option>
                    <option>19:</option>
                    <option>20:</option>
                    <option>21:</option>
                    <option>22:</option>
                    <option>23:</option>
                </select>
                <select id="eindminuutSelect">
                    <option>00</option>
                    <option>05</option>
                    <option>10</option>
                    <option>15</option>
                    <option>20</option>
                    <option>25</option>
                    <option>30</option>
                    <option>35</option>
                    <option>40</option>
                    <option>45</option>
                    <option>50</option>
                    <option>55</option>
                </select>
            </div>
            <div class="fieldDiv">
                <label class="labels" for="omschrijvingTextarea">Omschrijving</label>
                <textarea id="omschrijvingTextarea" cols="50" rows="5"></textarea><br />
            </div>
<%--            <div class="fieldDiv">
                <label class="labels" for="gefactureerdCheckBox">Gefactureerd</label>
                <input id="gefactureerdCheckBox" type="checkbox" /><br />
            </div>--%>
            <div class="fieldDiv">
                <input type="button" class="savebutton" value="Opslaan" id="opslaanbutton" onclick="OpslaanButton_Click();" />
            </div>
        </fieldset>
    </div>
    <h3>Tijdsregistraties</h3>
    <div id="registratiesDiv">
        <fieldset class="myfields">
            <div>
                <label for="datumTextbox">Datum</label>
                <input id="datumTextBox" class="datepicker" type="text" />
                <input id="updateButton" value="Update" type="button" onclick="updateButton_Click();" />
            </div>
        </fieldset>
        <table>
            <thead>
                <tr>
                    <th style='padding:5px 10px;'>Activiteit</th>
                    <th style='padding:5px 10px;'>Opdracht</th>
                    <th style='padding:5px 10px;'>Klant</th>
                    <th style='padding:5px 10px;'>Startuur</th>
                    <th style='padding:5px 10px;'>Einduur</th>
                    <th style='padding:5px 10px;'>Duur</th>
                    <th style='padding:5px 10px;'>Omschrijving</th>
                </tr>
            </thead>
            <tbody id="registratieTbody">

            </tbody>
        </table>
    </div>
</asp:Content>
